/* See LICENSE file for copyright and license details. */
#ifndef CONTROL__HH
#define CONTROL__HH

#include "mbed.h"

#include "config.h"

class Control
{
public:
	Control(BufferedSerial *pc, float *tang);
	virtual ~Control();

	int getangle();
private:
	int ref;
	float *ang;

	BufferedSerial *pc;
};

#endif

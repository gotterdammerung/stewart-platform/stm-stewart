/* See LICENSE file for copyright and license details. */
#include "control.h"

Control::Control(BufferedSerial *pc, float *tang):
pc(pc), ang(tang)
{
	int i;
	for (i=0; i<6; i++)
		ang[i] = motor_min + 0.5*(motor_max-motor_min);
}

Control::~Control()
{
}

int
Control::getangle()
{
	int i, j;
	char c, mess[4];
	int iang[6];

	/* Check data */
	for (i=0; i<6; i++)
	{
		for (j=0; j<3; j++)
			pc->read(&mess[j], 1);

		pc->read(&c, 1);

		if (((i<5)&&(c!=',')) || ((i==5)&&(c!=')')) )
			return -1;
		mess[3] = '\0';
		iang[i] = atoi(mess);
	}

	/* Convert to pulse */
	#ifdef DEBUG
	printf("[control] angle: ");
	#endif

	for (i=0; i<6; i=i+2)
	{
		ang[i] = motor_min + (iang[i]/180.0f)*(motor_max-motor_min) ;
		ang[i+1] = motor_min + (1-(iang[i+1]/180.0f))*(motor_max-motor_min);

		#ifdef DEBUG
		printf("(%03d,%3.2f) (%03d,%3.2f) ",
				iang[i], ang[i]*1e3, iang[i+1], ang[i+1]*1e3);
		#endif
	}

	#ifdef DEBUG
	printf("\n");
	#endif

	return 0;
}

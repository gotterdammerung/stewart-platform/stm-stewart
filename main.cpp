/* See LICENSE file for copyright and license details. */
#include "mbed.h"

#include "control.h"

#include "config.h"

/* Functions declarations */
void attime();

/* Declarations */
DigitalOut	led(LED1);
Ticker timer;

DigitalOut	pwm0(pwm_pin0);
DigitalOut	pwm1(pwm_pin1);
DigitalOut	pwm2(pwm_pin2);
DigitalOut	pwm3(pwm_pin3);
DigitalOut	pwm4(pwm_pin4);
DigitalOut	pwm5(pwm_pin5);


Control *control;

/* Protocols */
BufferedSerial	pc(USBTX, USBRX);

/* Globals */
float t;
float ang[6];

/* Events */
void
attime()
{
	/* Update time */
	t += tsample;
	if (motor_period<t)
		t = 0.0;

	/* first pulse */
	if (t<motor_min)
	{
		pwm0 = 1;
		pwm1 = 1;
		pwm2 = 1;
		pwm3 = 1;
		pwm4 = 1;
		pwm5 = 1;
		return;
	}

	/* Pulse duration */
	if (ang[0]<=t)
		pwm0 = 0;
	if (ang[1]<=t)
		pwm1 = 0;
	if (ang[2]<=t)
		pwm2 = 0;
	if (ang[3]<=t)
		pwm3 = 0;
	if (ang[4]<=t)
		pwm4 = 0;
	if (ang[5]<=t)
		pwm5 = 0;
}

int
main()
{
	char c;

	/* Protocols */
	pc.set_baud(serialbaudrate);

	timer.attach(&attime, TSAM);

	control = new Control(&pc, &ang[0]);

	while (1)
	{
		fflush(stdout);
		c = '\0';
		pc.read(&c, 1);
		switch (c)
		{
		case '(':
			if(!control->getangle())
				led = !led; /* Change LED if data is right */
			break;
		}
	}

	return 0;
}

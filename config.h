/* See LICENSE file for copyright and license details. */
#ifndef CONFIG_HH
#define CONFIG_HH

#define DEBUG

/* Serial port */
static const unsigned int serialbaudrate = 115200;

/* Sample time*/
#define TSAM	10us
static const float tsample = 10e-6;

/* PWM Pin */
static const PinName pwm_pin0 = D7;
static const PinName pwm_pin1 = D8;
static const PinName pwm_pin2 = D9;
static const PinName pwm_pin3 = D10;
static const PinName pwm_pin4 = D11;
static const PinName pwm_pin5 = D12;

/* Motor controller */
static const float motor_period = 20e-3; /* period */
static const float motor_min = 0.7e-3; /* min pulse */
static const float motor_max = 2.6e-3; /* max pulse */

#endif
